"use strict";
import {validateThongTinHoaDon, validateChiTietHoaDon} from "./validate.js";
import {hidenWhenKeyUp, hidenWhenKeyUp2, hidenWhenChang} from "./handelShowTitleError.js";

$(function () {
  const jqueryElement = {
    formThongTinHoaDon: $("#formThongTinHoaDon"),
    inputHoVaTen: $("#inputHoVaTen"),
    inputCMND: $("#inputCMND"),
    inputNgayXuatHoaDon: $("#inputNgayXuatHoaDon"),
    inputDiaChi: $("#inputDiaChi"),
    inputGhiChu: $("#inputGhiChu"),
    thongTinHoaDon: $("#thongTinHoaDon"),
    chiTietHoaDon: $("#chiTietHoaDon"),
    bodyChiTietHoaDon: $("#bodyChiTietHoaDon"),
    btnSuaThongTinHoaDon: $("#btnSuaThongTinHoaDon"),
    btnThemRow: $("#btnThemRow"),
    btnXuatHoaDon: $("#btnXuatHoaDon"),
    danhSachHoaDon: $("#danhSachHoaDon"),
  };
  const classCustomHiden = "custom-disabled";
  let dataThongTinHoaDon;

  // xoa hien thi loi khi nguoi dung nhap du lieu
  hidenWhenKeyUp([inputHoVaTen, inputDiaChi, inputGhiChu, inputCMND]);
  hidenWhenChang([inputNgayXuatHoaDon]);

  // click tao hoa don
  jqueryElement.formThongTinHoaDon.on("submit", function (e) {
    e.preventDefault();

    // validate
    const {isValid, data} = validateThongTinHoaDon(jqueryElement);
    if (isValid) {
      jqueryElement.chiTietHoaDon.removeClass(classCustomHiden);
      jqueryElement.thongTinHoaDon.addClass(classCustomHiden);
      renderRowChiTietHoaDon();
      dataThongTinHoaDon = data;
    }
  });

  // click chinh sua thong tin hoa don
  jqueryElement.btnSuaThongTinHoaDon.on("click", function (e) {
    jqueryElement.chiTietHoaDon.addClass(classCustomHiden);
    jqueryElement.thongTinHoaDon.removeClass(classCustomHiden);
    jqueryElement.bodyChiTietHoaDon.empty();
  });

  // click them row
  jqueryElement.btnThemRow.on("click", function (e) {
    // lay so luong phan tu trong the body
    let trLenght = jqueryElement.bodyChiTietHoaDon.children().length;
    jqueryElement.bodyChiTietHoaDon.append(renderTr(++trLenght));
  });

  // click xoa row
  $(document).on("click", function (e) {
    $(".fa-trash").on("click", function (e) {
      $(this).parent().parent().remove();
      renderRowChiTietHoaDon();
    });
  });
  // xoa danh sach don hang
  $(document).on("click", function (e) {
    $(".xoaDanhSach").on("click", function (e) {
      $(this).parent().parent().parent().remove();
      renderDanhSachHoaDon();
    });
  });

  // click xuat hoa don
  jqueryElement.btnXuatHoaDon.on("click", function (e) {
    let listChildrens = jqueryElement.bodyChiTietHoaDon.children();
    const listData = [];
    const listIsValis = [];
    listChildrens.each(function (e) {
      const {isValid, data} = validateChiTietHoaDon($(this));
      listIsValis.push(isValid);
      listData.push(data);
    });

    if (listIsValis.every((item) => item)) {
      dataThongTinHoaDon["chiTietHoaDon"] = listData;
      jqueryElement.danhSachHoaDon.append(renderDanhSachHoaDon(dataThongTinHoaDon));
    }

    hidenWhenKeyUp2(listChildrens);
  });

  // render noi dung trong chi tiet hoa don khi moi khoi tao
  function renderRowChiTietHoaDon() {
    // lay so luong phan tu trong the body
    let trLenght = jqueryElement.bodyChiTietHoaDon.children().length;
    while (trLenght++ < 2) {
      jqueryElement.bodyChiTietHoaDon.append(renderTr(trLenght));
    }
  }

  function renderTr(index) {
    return `
      <tr>
        <th scope="row">${index}</th>
        <td>
          <div class="form-group mb-0">
            <input type="text" class="form-control" data-type="tenHang" />
            <small class="form-text text-danger d-none">Help text</small>
          </div>
        </td>
        <td>
          <div class="form-group mb-0">
            <input type="text" class="form-control" data-type="soLuong" />
            <small class="form-text text-danger d-none">Help text</small>
          </div>
        </td>
        <td>
          <div class="form-group mb-0">
            <input type="text" class="form-control" data-type="donGia" />
            <small class="form-text text-danger d-none">Help text</small>
          </div>
        </td>
        <td>
          <button class="fa fa-trash"'></button>
        </td>
      </tr>
    `;
  }

  function renderDanhSachHoaDon(data) {
    const rowSpan = data.chiTietHoaDon.length;
    const day = data.inputNgayXuatHoaDon.getDate();
    const month = data.inputNgayXuatHoaDon.getMonth();
    const year = data.inputNgayXuatHoaDon.getFullYear();
    return `
      <tr>
        <th scope="row" rowspan="${rowSpan}">1</th>
        <td rowspan="${rowSpan}">${data.inputHoVaTen}</td>
        <td rowspan="${rowSpan}">${data.inputCMND}</td>
        <td rowspan="${rowSpan}">${day}/${month}/${year}</td>
        <td>${data.chiTietHoaDon[0].tenHangHoa}</td>
        <td>${data.chiTietHoaDon[0].soLuong}</td>
        <td>${data.chiTietHoaDon[0].donGia}</td>
        <td>${data.chiTietHoaDon[0].soLuong * data.chiTietHoaDon[0].donGia}</td>
        <td rowspan="${rowSpan}">${data.chiTietHoaDon.reduce((sum, item) => sum + item.soLuong * item.donGia, 0)}</td>
        <td rowspan="${rowSpan}">
          <button class="xoaDanhSach">delete</button>
        </td>
      </tr>
      ${renderChiTietHoaDon(data.chiTietHoaDon)}
    `;
  }

  function renderChiTietHoaDon(list) {
    let html = "";
    for (let i = 1; i < list.length; i++) {
      html += `
        <tr>
          <td>${list[i].tenHangHoa}</td>
          <td>${list[i].soLuong}</td>
          <td>${list[i].donGia}</td>
          <td>${list[i].donGia * list[i].soLuong}</td>
        </tr>;
      `;
    }
    return html;
  }
});
