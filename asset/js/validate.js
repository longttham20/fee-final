"use strict";
import {showMessageError} from "./handelShowTitleError.js";

function validateText(text) {
  const patten = /^[a-zA-Z0-9 ]+$/;
  return patten.test(text);
}

function validateCmnd(cmnd) {
  const patten = /^[0-9]{9}$/;
  return patten.test(cmnd);
}

function validateBeHonNgayHienTai(date) {
  return date.getTime() < new Date().getTime();
}

function validateNumber(number) {
  const patten = /^[1-9]+[0-9]*$/;
  return patten.test(number);
}

export function validateThongTinHoaDon(jqueryElement) {
  let isValid = true;
  let data = {
    inputHoVaTen: jqueryElement.inputHoVaTen.val(),
    inputCMND: jqueryElement.inputCMND.val(),
    inputNgayXuatHoaDon: new Date(jqueryElement.inputNgayXuatHoaDon.val()),
    inputDiaChi: jqueryElement.inputDiaChi.val(),
    inputGhiChu: jqueryElement.inputGhiChu.val(),
  };

  // validate ho ten
  if (data.inputHoVaTen.length === 0) {
    showMessageError("show", jqueryElement.inputHoVaTen, "Yêu cầu nhập họ tên");
    isValid = false;
  } else if (!validateText(data.inputHoVaTen)) {
    showMessageError("show", jqueryElement.inputHoVaTen, "Nhập không hợp lệ");
    isValid = false;
  }

  // validate dia chi
  if (data.inputDiaChi.length === 0) {
    showMessageError("show", jqueryElement.inputDiaChi, "Yêu cầu nhập địa chỉ");
    isValid = false;
  } else if (!validateText(data.inputDiaChi)) {
    showMessageError("show", jqueryElement.inputDiaChi, "Nhập không hợp lệ");
    isValid = false;
  }

  // validate ghi chu
  if (data.inputGhiChu.length === 0) {
    showMessageError("show", jqueryElement.inputGhiChu, "Yêu cầu nhập ghi chú");
    isValid = false;
  } else if (!validateText(data.inputGhiChu)) {
    showMessageError("show", jqueryElement.inputGhiChu, "Nhập không hợp lệ");
    isValid = false;
  }

  // validate cmnd
  if (data.inputCMND.length === 0) {
    showMessageError("show", jqueryElement.inputCMND, "Yêu cầu nhập CMND");
    isValid = false;
  } else if (!validateCmnd(data.inputCMND)) {
    showMessageError("show", jqueryElement.inputCMND, "Nhập không hợp lệ");
    isValid = false;
  }

  // validate ngay xuat hoa don
  if (jqueryElement.inputNgayXuatHoaDon.val().length === 0) {
    showMessageError("show", jqueryElement.inputNgayXuatHoaDon, "Yêu cầu nhập ngày tháng");
    isValid = false;
  } else if (!validateBeHonNgayHienTai(data.inputNgayXuatHoaDon)) {
    showMessageError("show", jqueryElement.inputNgayXuatHoaDon, "Nhập không hợp lệ");
    isValid = false;
  }

  isValid || (data = null);

  return {isValid, data};
}

export function validateChiTietHoaDon(jqueryTrElement) {
  const data = {};
  let isValid = true;
  const listTh = jqueryTrElement.children();
  const inputTenHangHoa = $(listTh[1]).find("input");
  const inputSoLuong = $(listTh[2]).find("input");
  const inputDonGia = $(listTh[3]).find("input");

  const classBorder = "border border-danger";
  const classDNone = "d-none";

  // validate ten hang hoa
  if (inputTenHangHoa.val().length === 0) {
    inputTenHangHoa.addClass(classBorder);
    const jSmall = $(listTh[1]).find("small");
    jSmall.text("Yêu cầu nhập tên hàng hoá");
    jSmall.removeClass(classDNone);
    isValid = false;
  } else if (!validateText(inputTenHangHoa.val())) {
    inputTenHangHoa.addClass(classBorder);
    const jSmall = $(listTh[1]).find("small");
    jSmall.text("Nhập không hợp lệ");
    jSmall.removeClass(classDNone);
    isValid = false;
  } else {
    data["tenHangHoa"] = inputTenHangHoa.val();
  }

  // validate so luong
  if (inputSoLuong.val().length === 0) {
    inputSoLuong.addClass(classBorder);
    const jSmall = $(listTh[2]).find("small");
    jSmall.text("Yêu cầu nhập số lượng");
    jSmall.removeClass(classDNone);
    isValid = false;
  } else if (!validateNumber(inputSoLuong.val())) {
    inputSoLuong.addClass(classBorder);
    const jSmall = $(listTh[2]).find("small");
    jSmall.text("Nhập không hợp lệ");
    jSmall.removeClass(classDNone);
    isValid = false;
  } else {
    data["soLuong"] = Number(inputSoLuong.val());
  }

  // validate don gia
  if (inputDonGia.val().length === 0) {
    inputDonGia.addClass(classBorder);
    const jSmall = $(listTh[3]).find("small");
    jSmall.text("Yêu cầu nhập đơn giá");
    jSmall.removeClass(classDNone);
    isValid = false;
  } else if (!validateNumber(inputDonGia.val())) {
    inputDonGia.addClass(classBorder);
    const jSmall = $(listTh[3]).find("small");
    jSmall.text("Nhập không hợp lệ");
    jSmall.removeClass(classDNone);
    isValid = false;
  } else {
    data["donGia"] = Number(inputDonGia.val());
  }

  return {isValid, data};
}
