"use strict";

//phuong thuc dung de hien thi noi dung thong bao o phia duoi the input
export function showMessageError(role, inputJqueryElement, message) {
  const ariaDescribedby = "aria-describedby";
  const classBorder = "border border-danger";
  const classTextWhite = "text-white";
  const classTextDanger = "text-danger";
  const smallJqueryElement = $(`#${inputJqueryElement.attr(ariaDescribedby)}`);
  if (role === "show") {
    inputJqueryElement.addClass(classBorder);
    smallJqueryElement.text(message);
    smallJqueryElement.addClass(classTextDanger);
    smallJqueryElement.removeClass(classTextWhite);
  } else {
    inputJqueryElement.removeClass(classBorder);
    smallJqueryElement.removeClass(classTextDanger);
    smallJqueryElement.addClass(classTextWhite);
  }
}

//phuong thuc dung de an di thong bao loi o duoi cac the input
export function hidenWhenKeyUp(jqueryElements) {
  jqueryElements.forEach((element) => {
    $(element).on("keyup", function (e) {
      showMessageError("hiden", $(this));
    });
  });
}

//phuong thuc dung de an di thong bao loi o duoi cac the input
export function hidenWhenKeyUp2(jqueryThElements) {
  const classBorder = "border border-danger";
  const classDNone = "d-none";
  jqueryThElements.each((index, element) => {
    $(element)
      .children()
      .find("input")
      .on("keyup", function (e) {
        $(this).removeClass(classBorder);
        $(this).parent().find("small").addClass(classDNone);
      });
  });
}

//phuong thuc dung de an di thong bao loi o duoi cac the input
export function hidenWhenChang(jqueryElements) {
  jqueryElements.forEach((element) => {
    $(element).on("change", function (e) {
      showMessageError("hiden", $(this));
    });
  });
}
